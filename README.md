These are the sources of my simplistic CPU implemented in an FPGA
(but usable in GHDL simulation)

# TO DO #
Assembler which may translate mnemonics into machine code like below:

    0000   NOP
    0000 SKOK1: NOP
    0000   NOP
    04e3   MOV #$1234, R3
    1234   ....
    0465   MOV R3,R5
    7c01   JMP SKOK1

Immediate operands should be marked with "#"
"$" should denote hexadecimal constants
Indirect addressing should be denoted by \[\] like below

    MOV #$5432,[R3]
    
The above means: Store 0x5432 to memory location pointed by R3.

Preincremantation should be denoted like below:

    MOV #$5432,+[R3]
    
The above means: Increment R3 and then store 0x5432 to memory location pointed by R3.


Postdecrementation should be denoted like below:

    MOV #$5432,[R3]-
    
The above means: Store 0x5432 to memory location pointed by R3 and decrement R3 later on.

The above may be used to implement stack. R6 should be initialized to 0xFFFF.
R6 points the highest __unused__ location on the stack.
To push R0 to stack one should do:

    MOV R0,[R6]-

To pop R0 from stack, one should do:

    MOV +[R6],R0
    
    


    
    

-------------------------------------------------------------------------------
-- Title      : BZCPUv1
-- Project    : bzcpu1
-------------------------------------------------------------------------------
-- File       : bzcpu.vhd
-- Author     : Bartosz M. Zabołotny  <bzab98@gmail.com>
-- Company    : 
-- Created    : 2016-07-27
-- Last update: 2016-07-27
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Simplistic CPU FPGA
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-07-27  1.0      bzab    Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;

entity bzcpu is

  port (
    addr_o : out std_logic_vector(15 downto 0);
    data_o : out std_logic_vector(15 downto 0);
    data_i : in  std_logic_vector(15 downto 0);
    mem_wr : out std_logic;
    io_rd  : out std_logic;
    io_wr  : out std_logic;
    err    : out std_logic;
    clk    : in  std_logic;
    rst_p  : in  std_logic);

end entity bzcpu;

architecture rtl of bzcpu is
  -- Working registers (R6 used as a stack pointer)
  type T_REGS is array (0 to 6) of std_logic_vector(15 downto 0);
  signal regs : T_REGS;

  type T_STATE is (ST_RESET, ST_DECODE, ST_HALT,
                   ST_FETCH, ST_FETCH0, ST_FETCH2,
                   ST_MOV_SRC, ST_MOV_DST, ST_MOV_DST_A,
                   ST_GET_OP1, ST_GET_OP1_A, ST_GET_OP1_B, ST_GET_OP1_C, ST_GET_OP1_D,
                   ST_JMP, ST_JMP1);
  signal ret_state, state : T_STATE               := ST_RESET;
  signal PC               : unsigned(15 downto 0) := (others => '0');
  signal opcode           : std_logic_vector(15 downto 0);
  signal op1, op2, res    : std_logic_vector(15 downto 0);

begin  -- architecture rtl

  p1 : process (clk) is
  begin  -- process p1
    if clk'event and clk = '1' then     -- rising clock edge
      mem_wr <= '0';
      io_rd <= '0';
      io_wr <= '0';
      if rst_p = '1' then               -- synchronous reset (active high)
        state  <= ST_RESET;
        err    <= '0';
        addr_o <= (others => '0');
        data_o <= (others => '0');
      else
        case state is
          when ST_RESET =>
            opcode <= data_i;
            addr_o <= std_logic_vector(PC);
            state  <= ST_FETCH;
          when ST_FETCH0 =>
            addr_o <= std_logic_vector(PC);
            state  <= ST_FETCH;
          when ST_FETCH =>
            state <= ST_FETCH2;
          when ST_FETCH2 =>
            opcode <= data_i;
            state  <= ST_DECODE;
          when ST_DECODE =>
            -- We decode the instruction read from data_i
            if std_match(opcode, "1"&"-----"&"----"&"---"&"---") then
            -- ALU command
            elsif std_match(opcode, "0"&"00001"&"----"&"---"&"---") then
              -- MOV command
              ret_state <= ST_MOV_DST;
              state <= ST_MOV_SRC;
            elsif std_match(opcode, "0"&"00010"&"----"&"---"&"---") then
              -- IOR command
              null;
            elsif std_match(opcode, "0"&"00011"&"----"&"---"&"---") then
              -- IOW command
              null;
            elsif std_match(opcode, "0"&"11111"&"0000"&"---"&"---") then
              -- JMP command
              case opcode(5 downto 3) is
                when "000" =>
                  state     <= ST_GET_OP1;
                  ret_state <= ST_JMP;
                when others =>
                  err   <= '1';
                  state <= ST_HALT;
              end case;
            elsif std_match(opcode, "0"&"11111"&"0001"&"---"&"---") then
              -- CAL command
              null;
            elsif std_match(opcode, "0"&"11111"&"0010"&"111"&"---") then
              -- RET command
              null;
            elsif std_match(opcode, "0"&"00000"&"0000"&"000"&"000") then
              -- NOP command
              PC    <= PC+1;              
              addr_o <= std_logic_vector(PC+1);
              state <= ST_FETCH;
            end if;
          when ST_JMP =>
            PC    <= unsigned(op1);
            addr_o <= std_logic_vector(unsigned(op1));
            state <= ST_FETCH;
          -- States related to fetching the destination address for JMP and CAL
          when ST_GET_OP1 =>
            case to_integer(unsigned(opcode(2 downto 0))) is
              when 0 to 5 =>
                op1   <= regs(to_integer(unsigned(opcode(2 downto 0))));
                state <= ST_JMP;
              when 6 =>
                addr_o <= std_logic_vector(PC+1);
                PC     <= PC+1;
                state  <= ST_GET_OP1_A;
              when 7 =>
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;
              when others => null;
            end case;
          when ST_GET_OP1_A =>
            state <= ST_GET_OP1_B;
          when ST_GET_OP1_B =>
            op1   <= data_i;
            state <= ret_state;
          when ST_GET_OP1_C =>
            state <= ST_GET_OP1_D;
          when ST_GET_OP1_D =>
            addr_o <= data_i;
            state  <= ST_GET_OP1_A;
          -- states related to fetching the SRC address for MOV
          when ST_MOV_SRC =>
            case to_integer(unsigned(opcode(9 downto 5))) is
              when 0 to 6 =>
                -- Registers and immediate
                op1   <= regs(to_integer(unsigned(opcode(9 downto 5))));
                state <= ret_state;                
              when 7 =>
                -- Immediate
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_A;
              when 8 to 14 => 
                -- Indirect register addressing
                addr_o <= regs(to_integer(unsigned(opcode(9 downto 5)))-8);
                state <= ST_GET_OP1_A;
              when 15 =>
                -- Indirect through memory pointer
                PC     <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state  <= ST_GET_OP1_C;                
              when 16 to 22 =>
                -- Indirect register with postincrementation
                regs(to_integer(unsigned(opcode(9 downto 5)))-16) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-16)) + 1);
                addr_o <= regs(to_integer(unsigned(opcode(9 downto 5)))-16);
                state <= ST_GET_OP1_A;                
              when 24 to 30 =>
                -- Indirect register with predecrementation
                regs(to_integer(unsigned(opcode(9 downto 5)))-24) <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)) - 1);
                addr_o <= std_logic_vector(unsigned(regs(to_integer(unsigned(opcode(9 downto 5)))-24)) - 1);
                state <= ST_GET_OP1_A;                
              when others => 
                state <= ST_HALT;
            end case;
          when ST_MOV_DST =>
            case to_integer(unsigned(opcode(4 downto 0))) is
              when 0 to 6 =>
                regs(to_integer(unsigned(opcode(4 downto 0)))) <= op1;
                PC <= PC+1;
                addr_o <= std_logic_vector(PC+1);
                state <= ST_FETCH;
              when 7 =>
                data_o <= op1;
                addr_o <= std_logic_vector(PC+1);
                PC     <= PC+1;
                ret_state <= ST_MOV_DST_A;
                state <= ST_GET_OP1_A;
              when others =>
                state <= ST_HALT;
            end case;
          when ST_MOV_DST_A =>
            addr_o <= op1;
            mem_wr <= '1';
            PC <= PC+1;
            state <= ST_FETCH0;            
          -- Error handling
          when ST_HALT =>
            err   <= '1';
            state <= ST_HALT;
          when others => null;
        end case;
      end if;
    end if;
  end process p1;


end architecture rtl;


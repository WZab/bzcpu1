# CPU Architecture #
* PC - program counter
* SP - stack pointer
* R0 to R6 - 7 working 16-bit registers 

# CPU Instruction Set #

What instructions do we need
* (1....) ALU - Arithmetical operations
    * Operations must be coded depending on the number of operands
	  In case of 2 operand operations, we must specify SRC1, SRC2 and DST
	  In case of a 1 operand operation, we need to specify only SRC and DST,
	  therefore, we can use SRC1 to specify the kind of operation.
	  * Operation (4-bits)
	    * (0000) AND
		* (0001) OR
		* (0010) XOR
		* (0011) ADD
		* (0100) ADC
		* (0101) SUB
		* (0110) SBC
		* (0111) CMP
		* (1000) ROL
		* (1001) ROR
		* (1010) LSR
		* (1011) ASR
		* (1100) SHL
		* (1110) MUL
	    * (1111) 1 op operation -specified in SRC1		
		  * (000) TST 
		  * (001) NOT
	      * (010) INC
		  * (011) DEC
		  * (100) NEG 
    * SRC1 encoding (3-bits)
	  * (nnn) Rn n=0..6, (111) Immediate
    * SRC2 encoding (3-bits)
	  * (nnn) Rn n=0..6, (111) Immediate
	* DST encoding (5-bits)
	  * (00nnn) Rn n=0..6, (00111) Immediate - address of destination
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the address of destination
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
	  	
	  * (00nnn) Rn n=0..6, (00111) Immediate - value
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect  - address of the value
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
* (0\_00001\_.... ) MOV - move data 
    * SRC encoding (5-bits)
	  * (00nnn) Rn n=0..6, (00111) Immediate - value
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the value, 
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
    * DST encoding (5-bits)
	  * (00nnn) Rn n=0..6 (00111) Immediate - address of destination
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the address of destination
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
* (0\_00010\_... ) IOR - read from IO
    * SRC encoding (5-bits)
	  * (00111) Immediate - value
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the value
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
    * DST encoding (5-bits)
	  * (00nnn) Rn n=0..6, (00111) Immediate - address of destination
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the address of destination
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation

* (0\_00011\_... ) IOW - write to IO
    * SRC encoding (5-bits)
	  * (00nnn) Rn n=0..6, (00111) Immediate - value
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect  - address of the value
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
    * DST encoding (5-bits)
	  * (00nnn) Rn n=0..6, (00111) Immediate - address of destination
	  * (01nnn) indirect Rn n=0..6, (01111) Indirect - address of the address of destination
	  * (10nnn) indirect Rn n=0..6 with preincrementation 
	  * (11nnn) indirect Rn n=0..6 with postdecrementation
* (0\_11111\_0000\_... ) JMP - Jump (either unconditional or conditional) 
    * 3-bits to encode type
      * (0) JMP unconditional
	  * (1) JMP if ZERO
	  * (2) JMP if not ZERO
	  * (3) JMP if CARRY
	  * (4) JMP if not CARRY
	  * (5) JMP if <0
	  * (6) JMP if >= 0
	* 3-bits to encode destination
	  * (0..5) R0-R5, 
	  * (6) Immediate (destination address follows the instruction)
	  * (7) Indirect (jump destination is taken from the memory pointed by the address following the instruction)
* (0\_11111\_0001\_... ) CAL - Call the subroutine (either unconditional or conditional) (3 bits to encode type)
    * 3-bits to encode type
      * (0) CAL unconditional
	  * (1) CAL if ZERO
	  * (2) CAL if not ZERO
	  * (3) CAL if CARRY
	  * (4) CAL if not CARRY
	  * (5) CAL if <0
	  * (6) CAL if >= 0
	* 3-bits to encode destination
	  * (0..5) R0-R5, 
	  * (6) Immediate (destination address follows the instruction)
	  * (7) Indirect (jump destination is taken from the memory pointed by the address following the instruction)
* (0\_11111\_0010\_111\_... RET) - Return from subroutine
    * 3-bits to encode type
      * (0) RET unconditional
	  * (1) RET if ZERO
	  * (2) RET if not ZERO
	  * (3) RET if CARRY
	  * (4) RET if not CARRY
	  * (5) RET if <0
	  * (6) RET if >= 0
* (all 0's) NOP



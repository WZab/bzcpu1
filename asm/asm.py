#!/bin/python
def pp(*args):
  print args

import pyparsing as p

def doAND():
   print "DoAND"
def doMOV():
   print "DoMOV"
def doJMP():
   print "DoJMP"


instructions = [
 ("AND", doAND),
 ("MOV", doMOV),
 ("JMP", doJMP),
]

tLabel = p.Word( p.alphas, p.alphanums ).setResultsName("label")
tLabelDef = p.Optional(p.LineStart()+tLabel+":")
# Each complex element returns the list of parsing results. That's why in case of hexadecimal numbers we have to use t[1] not t[0]
tNumber = (p.Word( p.nums ).setParseAction(lambda t: int(t[0]))) ^ ("$"+p.Word( p.hexnums )).setParseAction(lambda t: int(t[1],16))
tComment = p.Optional(";"+ p.ZeroOrMore(p.Word( p.printables )))
tOper = p.Or([p.Keyword(i[0]).setParseAction(i[1]) for i in instructions]).setResultsName("code")
tR = ("R"+p.Word(p.srange("[0-6]"))).setParseAction(lambda t: t[1])
#
tArgR = tR.setResultsName("argr")
tArgImm = "#"+(tNumber.setResultsName("argi_num") ^ tLabel.setResultsName("argi_lab"))
tArgImmDst = (tNumber.setResultsName("argid_num") ^ tLabel.setResultsName("argid_lab"))
tArgRInd = "["+tR.setResultsName("argr_ind")+"]"
tArgInd = "["+(tNumber.setResultsName("argind_num") ^ tLabel.setResultsName("argind_lab"))+"]"
tArgRIndPreI = "+["+tR.setResultsName("argr_prei")+"]"
tArgRIndPostD = "["+tR.setResultsName("argr_postd")+"]-"
tArg = p.Group(tArgR ^ tArgRInd ^ tArgRIndPreI ^ tArgRIndPostD ^ tArgImm ^ tArgImmDst ^ tArgInd)
tLine0Arg = tLabelDef+tOper+tComment+p.LineEnd()
tLine1Arg = tLabelDef+tOper+tArg.setResultsName("a1")+tComment+p.LineEnd()
tLine2Arg = tLabelDef+tOper+tArg.setResultsName("a1")+","+tArg.setResultsName("a2")+tComment+p.LineEnd()
tLine3Arg = tLabelDef+tOper+tArg.setResultsName("a1")+","+tArg.setResultsName("a2")+","+tArg.setResultsName("a3")+tComment+p.LineEnd()
tLine = tLine0Arg ^ tLine1Arg ^ tLine2Arg ^ tLine3Arg


